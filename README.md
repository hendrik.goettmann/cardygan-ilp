# CardyGAn Integer Linear Programming (ILP) - Library

CardyGAn ILP provides a Java interface to integer linear programming solvers (ilp) with a rich API supporting propositional combination of (integer) linear arithmetic constraints.

## Compiling the project
We use gradle for building the project (https://gradle.org). Take the following steps to build the project:
1. Create a file `gradle.properties` in the project root based on the provided sample.
```
cp gradle.properties.sample gradle.properties
```
2. As content of `gradle.properties` add the following line and adapt the path according to your local setup:
```
cplex.home=/opt/ibm/ILOG/CPLEX_Studio128
cplex.arch=x86-64_linux
gurobi=/opt/gurobi912/linux64/lib
```
3. In the project root execute the command `./gradlew jar` from the command line to build the library.

## IntelliJ IDE
For unknown reason IntelliJ cannot run the tests directly.
Apparently, something is wrong with the environment variables
leading to a failure in the initialization process of Gurobi.
However, executing `./gradlew test` in the terminal
(even the one integrated into IntelliJ)
works perfectly fine.

## CPLEX-specific Setup
To use cplex solver you can either pass the native library path using the constructor or via the environment variable ```CPLEX_LIB_PATH```. Example for setting the environment variable in Unix:
```
export CPLEX_LIB_PATH=/Users/user1/Applications/IBM/ILOG/CPLEX_Studio1263/cplex/bin/x86-64_osx/
```

Additionally you need to put the solver specific JNI jar (`cplex.jar`) on the classpath.

## Usage
See the class `src/test/java/org/cardygan/ilp/org.cardygan.ilp.api.SampleApiUsage.java` for sample usage.
